var gulp = require('gulp');
var eslint = require('eslint/lib/cli');
var plugins = require('gulp-load-plugins')();
var del = require('del');
var es = require('event-stream');
var bowerFiles = require('main-bower-files');
var browserSync = require('browser-sync');
var watch = require('gulp-watch');

var log = function (error) {
    console.log([
        '',
        "----------ERROR MESSAGE START----------",
        ("[" + error.name + " in " + error.plugin + "]"),
        error.message,
        "----------ERROR MESSAGE END----------",
        ''
    ].join('\n'));
    this.end();
};

var scriptPaths = [
    'app/**/*.module.js',
    'app/**/*.run.js',
    'app/**/*.config.js',
    'app/**/*.controller.js',
    'app/**/*.service.js',
    'app/**/*.filter.js',
    'app/**/*.value.js',
    'app/**/*.directive.js'
];


var paths = {
    scripts: scriptPaths,
    styles: ['./app/scss/**/*.css', './app/scss/**/*.scss'],
    partialsJade: ['app/**/*.jade', '!app/index.jade'],
    indexJade: 'app/index.jade',
    partials: ['app/**/*.html', '!app/index.html'],
    distDev: 'dist.dev',
    distDevCss: 'dist.dev/css'
};

var pipes = {};
pipes.orderedVendorScripts = function () {
    return plugins.order(['jquery.js', 'angular.js']);
};
pipes.validatedAppScripts = function () {
    return gulp.src(paths.scripts)
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'));
};

pipes.buildIndexFile = function () {
    return gulp.src(paths.indexJade)
        .pipe(plugins.plumber({
            errorHandler: function (error) {
                console.log(error.message);
            }
        }))
        .pipe(plugins.jade())
        .pipe(plugins.prettify({indent_size: 2}))
};

pipes.builtVendorScriptsDev = function () {
    return gulp.src(bowerFiles({
        includeDev: true
    }))
        .pipe(gulp.dest(paths.distDev + '/bower_components'));
};

pipes.builtVendorStyleDev = function () {
    return gulp.src(bowerFiles({
        includeDev: true
    }))
        .pipe(gulp.dest(paths.distDev + '/bower_components'));
};

pipes.builtAppScriptsDev = function () {
    return gulp.src(paths.scripts)
        .pipe(plugins.concat('app.js'))
        .pipe(gulp.dest(paths.distDev));
};

pipes.builtStylesDev = function () {
    return gulp.src(paths.styles)
        .pipe(plugins.plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }
        }))
        .pipe(plugins.sass().on('error', plugins.sass.logError))

        .pipe(gulp.dest(paths.distDevCss));
};

pipes.builtPartialsFilesDev = function () {
    return gulp.src(paths.partialsJade)
        .pipe(plugins.plumber())
        .pipe(plugins.jade())
        .pipe(plugins.prettify({indent_size: 2}))
        .pipe(gulp.dest(paths.distDev));
};

pipes.builtIndexDev = function () {
    var orderedVendorScripts = pipes.builtVendorScriptsDev()
        .pipe(pipes.orderedVendorScripts());
    var appVendorStyle = pipes.builtVendorScriptsDev();

    var orderedAppScripts = pipes.builtAppScriptsDev();
    var appStyles = pipes.builtStylesDev();
    return pipes.buildIndexFile()
        .pipe(gulp.dest(paths.distDev)) // write first to get relative path for inject
        .pipe(plugins.inject(orderedVendorScripts, {relative: true, name: 'bower'}))
        .pipe(plugins.inject(orderedAppScripts, {relative: true}))
        .pipe(plugins.inject(appStyles, {relative: true}))
        .pipe(gulp.dest(paths.distDev));
};

pipes.builtAppDev = function () {
    return es.merge(pipes.builtIndexDev(), pipes.builtPartialsFilesDev());
};

gulp.task('clean-dev', function () {
    return del(paths.distDev);
});

gulp.task('build-app-dev', pipes.builtAppDev);

gulp.task('clean-build-app-dev', ['clean-dev'], pipes.builtAppDev);
gulp.task('watch-dev', ['clean-build-app-dev'], function () {
    var indexPath;
    var partialsPath;
    var reload = browserSync.reload;

    indexPath = paths.indexJade;
    partialsPath = paths.partialsJade;
    // start browser-sync to auto-reload the dev server
    browserSync({
        port: 8000,
        server: {
            baseDir: paths.distDev
        }
    });

    // watch index
    watch(indexPath, function () {
        return pipes.builtIndexDev()
            .pipe(reload({stream: true}));
    });

    // watch app scripts
    watch(paths.scripts, function () {
        return pipes.builtAppScriptsDev()
            .pipe(reload({stream: true}));
    });

    // watch html partials
    watch(partialsPath, function () {
        return pipes.builtPartialsFilesDev()
            .pipe(reload({stream: true}));

    });

    // watch styles
    watch(paths.styles, function () {
        return pipes.builtStylesDev()
            .pipe(reload({stream: true}));
    });

});
gulp.task('default', ['watch-dev']);

gulp.task('eslint', function (done) {
    eslint.execute(paths.scripts.join(' '));
    done();
});