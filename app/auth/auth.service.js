(function () {
    'use strict';

    angular
        .module('app.auth')
        .factory('auth', auth);

    /* @ngInject */


    function auth($http, localStorageService,$location, $q) {
        var service = {
                login: login,
                logout: logout,
                IsAuthenticated: IsAuthenticated,
                getUserName: getUserName
            },
            user,
            storage_name = 'authorizationData';

        activate();

        return service;

        // ---- functions ----

        function activate() {
            user = localStorageService.get(storage_name);
        }

        function login(user_data) {
            return $http.post('/auth', user_data)
                .then(function (val) {
                    user = val.data;
                    localStorageService.set(storage_name, user);
                });
        }

        function IsAuthenticated() {
            return !!user;
        }

        function logout() {
            user = undefined;
            localStorageService.remove(storage_name);
            $location.path('/login');
        }

        function getUserName() {
            return user.username;
        }
    }
})();