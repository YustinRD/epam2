(function () {
    'use strict';

    angular
        .module('app.courses')
        .controller('CourseCtrl', CourseCtrl);

    /* @ngInject */

    function CourseCtrl($scope, $routeParams, courses, $location, authorsList) {
        var vm = this,
            id = parseInt($routeParams.id);

        vm.authors = authorsList;

        activate();
        ///

        vm.save = save;

        function activate() {
            vm.form = {
                name: ''
            };

            if (!isNaN(id)) {
                load();
            }
            else {
                setWatch();
            }
        }

        function load() {
            courses.get({id: id}, function (course) {
                vm.form = course;
                setWatch();
            });
        }

        function save() {
            if ($scope.form.$valid) {
                if (isNaN(id)) {
                    var course = new courses(vm.form);
                    course.$save(function () {
                        $location.path('courses');
                    });
                }
                else {
                    vm.form.$update(function () {
                        $location.path('courses');
                    });
                }
            }
        }

        function setWatch() {
            $scope.$watch(function () {
                return vm.form.name;
            }, function (val) {
                $scope.title = val;
            });
        }
    }
})();