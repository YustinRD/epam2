(function () {
    'use strict';

    angular.module('app.courses', [
        'ngResource',
        'common',
        'ui.bootstrap'
    ]);
})();