(function () {
    'use strict';

    describe('CoursesCtrl', function () {
        beforeEach(module('app'));
        beforeEach(module('ngMockE2E'));

        var courses,
            $uibModal,
            vm,
            $q,
            $rootScope,
            data = [
                {
                    id: 1,
                    name: '1'
                },
                {
                    id: 2,
                    name: '2'
                }
            ];

        beforeEach(inject(function (_$uibModal_, $controller, _$q_, _$rootScope_) {
            $uibModal = _$uibModal_;
            $rootScope = _$rootScope_;
            $q = _$q_;

            courses = {};
            courses.query = function (cb) {
                $q.when(data).then(function (data) {
                    cb(data);
                });
            };


            vm = $controller('CoursesCtrl', {
                courses: courses
            });

            spyOn(vm, 'courses');
            spyOn($uibModal, 'open').and.callThrough();

            $rootScope.$digest();
        }));

        it('should activate() load courses list at start', function () {
            // Assert
            expect(vm).toBeDefined();
            expect(vm.courses).toBeDefined();
            expect(vm.courses.length).toBe(data.length);
        });

        it('should open modal window', function(){
            //Act
            vm.deleteCourse({name:'test'});
            //Assert
            expect($uibModal.open).toHaveBeenCalled();
        });
    });
})();