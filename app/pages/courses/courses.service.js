(function () {
    'use strict';

    angular
        .module('app.courses')
        .factory('courses', courses);

    /* @ngInject */

    function courses($resource) {
        return $resource('/courses/:id', null, {
                'update': {method: 'PUT'}
            }
        );
    }
})();