(function () {
    'use strict';

    angular
        .module('app.courses')
        .factory('authors', authors);

    /* @ngInject */

    function authors($resource) {
        return $resource('/authors/:id', null, {
                'update': {method: 'PUT'}
            }
        );
    }
})();