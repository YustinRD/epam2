(function () {
    'use strict';

    angular
        .module('app.courses')
        .run(runFunc);

    var test_data = [

            {
                id: 10,
                name: 'Видеокурс 10',
                date: '2016-08-03T07:46:38.326Z',
                duration: 134,
                description: ' Описание3',
                authors: [
                    'Roman Yustin',
                    'Dmitriy Ivanov'
                ]

            },
            {
                id: 1,
                name: 'Видеокурс 1',
                date: '2016-07-01T07:46:38.326Z',
                duration: 90,
                description: ' Описание1',
                authors: [
                    'Roman Yustin',
                    'Dmitriy Ivanov',
                    'Mikhail_Semichev'
                ]
            },
            {
                id: 2,
                name: 'Видеокурс 2',
                date: '2016-04-23T07:46:38.326Z',
                duration: 70,
                description: ' Описание2',
                authors: [
                    'Roman Yustin',
                    'Dmitriy Ivanov'
                ]

            },
            {
                id: 3,
                name: 'Видеокурс 3',
                date: '2016-02-03T07:46:38.326Z',
                duration: 134,
                description: ' Описание3',
                authors: [
                    'Roman Yustin',
                    'Dmitriy Ivanov'
                ]

            },
            {
                id: 11,
                name: 'Видеокурс 4',
                date: '2016-01-01T07:46:38.326Z',
                duration: 10,
                description: ' Описание3',
                authors: [
                    'Roman Yustin',
                    'Dmitriy Ivanov'
                ]

            }
        ],
        authors = [
            'Roman Yustin',
            'Dmitriy Ivanov',
            'Mikhail_Semichev'
        ],
        id = 12;

    /* @ngInject */

    function runFunc($httpBackend) {
        var regexp = new RegExp('\\/courses\\/([0-9]+)');

        $httpBackend.whenGET('/courses')
            .respond(function () {
                return [200, test_data];
            });

        $httpBackend.whenGET(regexp)
            .respond(function (method, url) {
                var id = url.match(regexp)[1],
                    course = test_data.filter(function (item) {
                        return item.id == id;
                    });

                return [200, course[0]];
            });

        $httpBackend.whenPOST('/courses')
            .respond(function (method, url, data) {
                data = JSON.parse(data);
                data.id = id++;
                test_data.push(data);

                return [200, data];
            });

        $httpBackend.whenPUT('/courses')
            .respond(function (method, url, data) {
                data = JSON.parse(data);
                if (data.id) {
                    var index = test_data.findIndex(function (item) {
                        return item.id === data.id;
                    });
                    test_data[index] = data;
                    return [200, data];
                }
            }
        );

        $httpBackend.whenDELETE(regexp)
            .respond(function (method, url) {
                var id = url.match(regexp)[1];
                id = parseInt(id);
                test_data = test_data.filter(function (item) {
                    return item.id !== id;
                });
                return [200, 'OK'];
            });

        $httpBackend.whenGET('/authors')
            .respond(function () {
                return [200, authors];
            });
    }

})
();