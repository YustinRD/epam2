(function () {
    'use strict';

    angular
        .module('app.courses')
        .config(config);

// @ngInject
    function config($routeProvider) {
        var course = {
            name: 'Курсы',
            link: '#/courses'
        };

        $routeProvider
            .when('/courses', {
                templateUrl: 'pages/courses/courses.html',
                controller: 'CoursesCtrl',
                controllerAs: 'CoursesCtrl',
                breadcrumb: [course],
                resolve: {loginRequired: loginRequired}
            });

        $routeProvider
            .when('/courses/:id', {
                templateUrl: 'pages/courses/course.html',
                controller: 'CourseCtrl',
                controllerAs: 'CourseCtrl',
                breadcrumb: [course],
                resolve: {
                    loginRequired: loginRequired,
                    authorsList: authors
                }
            });
    }

    // @ngInject
    function loginRequired($location, $q, auth) {
        var deferred = $q.defer();
        if (auth.IsAuthenticated()) {
            deferred.resolve();
        }
        else {
            deferred.reject();
            $location.path('/login');
        }
        return deferred.promise;
    }

    // @ngInject
    function authors(authors) {
        return authors.query();
    }
})();
