(function () {
    'use strict';

    angular
        .module('app.courses')
        .controller('CoursesCtrl', CoursesCtrl);

    /* @ngInject */

    function CoursesCtrl(courses, $uibModal) {
        var vm = this;

        activate();

        // ---- functions ----

        vm.deleteCourse = deleteCourse;

        function activate() {
            vm.courses = [];
            loadList();
        }

        function loadList() {
            courses.query(function (list) {
                vm.courses = list;
            });
        }

        function deleteCourse(course) {
            var modalInstance = $uibModal.open({
                templateUrl: '/pages/courses/delete.html',
                controller: 'deleteCtrl',
                controllerAs: 'deleteCtrl',
                size: 'sm',
                resolve: {
                    name: function () {
                        return course.name;
                    }
                }
            });

            modalInstance.result.then(function () {
                course.$delete({id: course.id}, function () {
                    loadList();
                });
            });
        }
    }
})();