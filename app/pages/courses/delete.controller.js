(function () {
    'use strict';

    angular
        .module('app.courses')
        .controller('deleteCtrl', deleteCtrl);

    /* @ngInject */

    function deleteCtrl($uibModalInstance, name) {
        var vm = this;
        vm.name = name;

        ///

        vm.ok = ok;
        vm.cancel = cancel;

        function ok() {
            $uibModalInstance.close();
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();