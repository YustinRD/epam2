(function () {
    'use strict';

    angular
        .module('app.login')
        .controller('LoginCtrl', LoginCtrl);

    /* @ngInject */

    function LoginCtrl($scope, auth, $location) {
        var vm = this;

        vm.regLogin = /^[A-z]+$/i;
        vm.regPassword = /^[a-zA-Z0-9]+$/i;
        vm.checkLogin = true;

        ///

        vm.login = login;


        function login() {
            auth.login(vm.user)
                .then(function (data) {
                    $location.path('courses');
                },function(){
                    vm.checkLogin = false;
                });
        }

    }
})();