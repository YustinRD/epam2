(function () {
    'use strict';

    angular
        .module('app.login')
        .config(config);

// @ngInject
    function config($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'pages/login/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'LoginCtrl',
                resolve: {redirectIfAuthenticated: redirectIfAuthenticated('/courses')}
            });
    }

    // @ngInject
    function redirectIfAuthenticated(route, $location, $q, auth) {
        return function ($location, $q, auth) {
            var deferred = $q.defer();
            if (auth.IsAuthenticated()) {
                deferred.reject();
                $location.path(route);
            }
            else {
                deferred.resolve();
            }
            return deferred.promise;
        };
    }
})();
