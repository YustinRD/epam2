(function () {
    'use strict';

    angular
        .module('app.login')
        .run(runFunc);

    /* @ngInject */

    function runFunc($httpBackend) {

        $httpBackend.whenPOST('/auth')
            .respond(function (method, url, data) {
                data = JSON.parse(data);

                if (data.username === 'admin' && data.password === 'admin') {
                    return [200, {
                        username: data.username,
                        token: '1122233'
                    }];
                }

                return [401, 'user not found'];
            });
    }
})();