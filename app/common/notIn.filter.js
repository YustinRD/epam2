(function () {
    'use strict';

    angular
        .module('common')
        .filter('notIn', notIn);

    /* @ngInject */

    function notIn() {
        return function (arr, arr2) {
            arr2 = arr2 || [];
            arr = arr.filter(function (item) {
                if(arr2.indexOf(item) < 0){
                    return item;
                }
            });
            return arr;
        };
    }
})();