(function () {
    'use strict';

    angular
        .module('common')
        .directive('app2list', app2list);

    /* @ngInject */

    function app2list() {
        var directive = {
                restrict: 'A',
                templateUrl: '/common/2list.html',
                require: 'ngModel',
                scope: {
                    list2: '=app2list',
                    list: '=ngModel'
                },
                bindToController: true,
                controller: CtrlDirective,
                controllerAs: 'vm',
                link: linkFunc
            },
            vm;

        return directive;

        function CtrlDirective($scope) {
            vm = this;
        }

        function linkFunc(scope, el, attr, ctrl) {
            vm.add = add;
            vm.remove = remove;


            ctrl.$isEmpty = function (val) {
                return !(val && val.length);
            };

            function add() {
                vm.list = vm.list || [];

                angular.forEach(vm.select2, function (val) {
                    vm.list.push(val);
                });
                vm.select2 = null;
            }

            function remove() {
                var list1 = vm.select1 || [],
                    val = vm.list.filter(function (val) {
                        return list1.indexOf(val) < 0;
                    });

                vm.select1 = null;

                val = val.length === 0 ? [] : val;
                ctrl.$setViewValue(val);
            }
        }
    }
})();