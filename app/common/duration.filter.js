(function () {
    'use strict';

    angular
        .module('common')
        .filter('duration', duration);

    function duration() {
        return function (item) {
            var minutes = parseInt(item),
                hour = Math.floor(minutes / 60),
                result = '';

            if (isNaN(minutes)) {
                return '';
            }
            minutes %= 60;

            if (hour) {
                result += hour + ' ' + getHour(hour);
            }

            if (minutes) {
                result = [result, minutes, 'мин'].join(' ');
            }

            return result;
        };
    }

    function getHour(h) {
        if (h > 4 && h < 21) {
            return 'часов';
        }

        switch (h % 10) {
            case 1:
                return 'час';
            case 2:
            case 3:
            case 4:
                return 'часа';
            default:
                return 'часов';
        }

    }

})();