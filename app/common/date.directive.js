(function () {
    'use strict';

    angular
        .module('common')
        .directive('appDate', appDate);

    function appDate() {
        var directive = {
                restrict: 'A',
                require: 'ngModel',
                link: linkFunc
            },
            regexp = /^(\d(\d(\.(\d(\d(\.(\d(\d(\d(\d)?)?)?)?)?)?)?)?)?)?$/;

        return directive;

        function linkFunc(scope, el, attr, ctrl) {
            var rollback = ctrl.$viewValue;
            ctrl.$parsers.push(toDate);
            ctrl.$parsers.unshift(parser);
            ctrl.$formatters.unshift(format);

            ///

            ctrl.$validators.date = function (val) {
                return val || !ctrl.$viewValue;
            };

            function parser(val) {
                if (!checkDate(val)) {
                    val = rollback;
                    ctrl.$setViewValue(val);
                    ctrl.$render();
                }
                rollback = val;
                return val;
            }

            function toDate(val) {
                var str = format(val),
                    valid = val === str,
                    date = strToDate(val);


                return valid ? date : null;
            }

            function format(val) {
                var date = strToDate(val),
                    result = [];
                if (date) {
                    result.push(('0' + date.getDate()).slice(-2));
                    result.push(('0' + (date.getMonth() + 1)).slice(-2));
                    result.push(date.getFullYear());
                    return result.join('.');
                }
                return val;
            }

            function strToDate(val) {
                var arr = val ? val.match(/^(\d\d)\.(\d\d)\.(\d\d\d\d)$/) : null;
                if (arr) {
                    return new Date(arr[3], arr[2] - 1, arr[1]);
                }
                return null;
            }

            function checkDate(str) {
                return regexp.test(str);
            }
        }
    }
})();