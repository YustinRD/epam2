(function () {
    'use strict';

    angular
        .module('app')
        .directive('appBreadcrumb', appBreadcrumb);

    /* @ngInject */

    function appBreadcrumb($route) {
        var directive = {
            restrict: 'A',
            templateUrl: '/common/breadcrumb.html',
            scope: {},
            link: linkFunc,
            controller: Ctrl,
            controllerAs: 'vm'
        };

        return directive;


        function Ctrl() {
            var vm = this;
            vm.setList = setList;

            function setList(title) {
                vm.list = getBreadcrumb();
                if (title) {
                    vm.list.push({
                        name: title
                    });
                }
            }

            function getBreadcrumb() {
                var route = $route.current;

                return route ? angular.copy(route.breadcrumb) || [] : [];
            }
        }

        function linkFunc(scope, el, attr, vm) {
            var watch = function () {
            };

            scope.$on('$routeChangeSuccess', function () {
                watch();
                watch = scope.$watch(function () {
                    return $route.current.scope ? $route.current.scope.title : null;
                }, function (val) {
                    vm.setList(val)
                });
            });
        }
    }
})();