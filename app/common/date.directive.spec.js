(function () {
    'use strict';

    describe('dateDirective', function () {
        beforeEach(module('app'));
        beforeEach(module('common'));
        beforeEach(module('ngMockE2E'));

        var scope,
            element,
            $rootScope,
            form;

        beforeEach(inject(function ($compile, _$rootScope_) {
            var html = '<form name="form"><input ng-model="model" name="date" app-date required></form>';
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();

            element = $compile(html)(scope);
            form = scope.form;
        }));

        it('should parse valid date', function () {
            //Arrange
            var strDate = '12.01.2015',
                date;

            //Act
            date = new Date(2015, 0, 12);
            form.date.$setViewValue(strDate);

            //Assert
            expect(scope.model).toEqual(date);
            expect(form.date.$valid).toBe(true);
        });

        it('should return to a previous state', function () {
            //Arrange
            var firstState = '12',
                secondState = '122';

            //Act
            form.date.$setViewValue(firstState);
            form.date.$setViewValue(secondState);

            //Assert
            expect(form.date.$viewValue).toEqual(firstState);
        });

        it('should be an invalid value', function () {
            //Arrange
            var date = '12.13.2015';

            //Act
            form.date.$setViewValue(date);

            //Assert

            //scope.$digest();
            expect(form.date.$error.date).toBe(true);
        });

        it('incomplete should be invalid', function () {
            //Arrange
            var date = '12.1';

            //Act
            form.date.$setViewValue(date);

            //Assert
            expect(form.date.$error.date).toBe(true);
        });

        it('empty value should be valid', function () {
            //Arrange
            var date = '';

            //Act
            form.date.$setViewValue(date);

            //Assert
            expect(form.date.$error.date).toBeFalsy();
        });
    });
})();