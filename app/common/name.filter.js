(function () {
    'use strict';

    angular
        .module('common')
        .filter('byName', byName);

    /* @ngInject */

    function byName() {
        return function (items, search) {
            var new_arr = [];
            if (search) {
                search = search.toLowerCase();
            }
            if (items) {
                angular.forEach(items, function (item) {
                    if (search === undefined || item.name !== undefined && item.name.toLowerCase().indexOf(search) > -1) {
                        new_arr.push(item);
                    }
                });
            }
            return new_arr;
        };
    }
})();