(function () {
    'use strict';

    angular.module('app.core', [
        'app.auth',
        'app.courses',
        'app.login'
    ]);
})();