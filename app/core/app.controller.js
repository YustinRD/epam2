(function () {
    'use strict';

    angular
        .module('app')
        .controller('MainCtrl', Ctrl);

    /* @ngInject */

    function Ctrl(auth) {
        var vm = this;

        ///
        vm.getUserName = getUserName;
        vm.IsAuthenticated = IsAuthenticated;
        vm.logout = logout;

        function getUserName() {
            return auth.getUserName();
        }

        function IsAuthenticated() {
            return auth.IsAuthenticated();
        }

        function logout() {
            auth.logout();
        }
    }
})();