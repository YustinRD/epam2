(function () {
    'use strict';

    angular
        .module('app')
        .run(runFunc);

    /* @ngInject */

    function runFunc($httpBackend) {
        $httpBackend.whenGET(/\.html$/).passThrough();
    }
})();
