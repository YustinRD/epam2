(function () {
    'use strict';

    angular
        .module('app', [
            'ngRoute',
            'ngMockE2E',
            'app.core'
        ]);
})();