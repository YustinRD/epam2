(function () {
    'use strict';

    angular
        .module('app')
        .config(config);

// @ngInject
    function config($routeProvider) {
        $routeProvider
            .otherwise( {
                redirectTo : '/login'
            });
    }
})();
